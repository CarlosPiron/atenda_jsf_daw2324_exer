package backing;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;
import java.util.ResourceBundle;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebFilter("/atenda/*")
public class AuthFilter implements Filter {
	private static final ResourceBundle config= ResourceBundle.getBundle("config");
	// keys para config
	private static final String ROL_BASIC= "rol.BASIC";
	private static final String ROL_ANON= "rol.ANON";
	private static final String ROL_ADMIN= "rol.ADMIN";
	private static final String UPLOAD_DIR= "upload.dir";
	///////
	private HttpServletRequest httpRequest;
	private Hashtable<String, String[]> taboaAutorizacions;

	@Inject
	private Credenciais credenciais;

	public AuthFilter() {
	}
	public void destroy() {
	}
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		// para logging
		//System.out.println("AuthFilter:: username: " + credenciais.getUsername() + "" + " rol: " + credenciais.getRol());
                //////// fin logging

		httpRequest = (HttpServletRequest) request;
		String path = httpRequest.getRequestURI().substring(httpRequest.getContextPath().length());

		// elimino jsessionid
		if (path.lastIndexOf("jsessionid") > 0) {
			path = path.substring(0, path.lastIndexOf("jsessionid") - 1);
			//System.out.println("Path filtrado:: " + path);
		}

		// para logging
		//System.out.println("Path:::::: " + path);
        ///////// fin logging

		if (credenciais.getUsername() == null) {
			credenciais.setUsername("");
			credenciais.setRol(config.getString(ROL_ANON));
			credenciais.setAutenticado(false);
		}
		boolean isAuthorised = isAuthorised(credenciais.getRol(), path);
		// fin logging
		// para logging
		if (isAuthorised) {
			//System.out.println("User " + credenciais.getRol() + " autorizado para: " + path);
		} else {
			//System.out.println("User " + credenciais.getRol() + " NO autorizado para: " + path);
		}
          //////////////////// fin logging

		if (!isAuthorised ) {
			//httpRequest.getRequestDispatcher("/").forward(request, response);
			((HttpServletResponse)response).sendRedirect( ((HttpServletRequest)request).getContextPath()+"/index.xhtml");
		} else {
			// se está autorizado, invocamos ao seguinte filtro na cadea
			chain.doFilter(request, response);
		}
		/// PARA PROBAS 
	    //chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		// construo a taboa de autorizacións que conten para cada rol
		// unha lista das urls autorizadas
		taboaAutorizacions = new Hashtable<String, String[]>();
		String[] urlsADMIN = { "/favicon.ico","/images/", "/index.xhtml", "/admin.xhtml",  "/perfil.xhtml"};
		String[] urlsBASIC = { "/favicon.ico",  "/images/","/index.xhtml", "/carro.xhtml", "/pedidos.xhtml", "/devolucion.xhtml", "/perfil.xhtml"};
		String[] urlsANON = { "/favicon.ico",  "/images/","/index.xhtml" , "/carro.xhtml", "/rexistro.xhtml"};
		
		taboaAutorizacions.put("BASIC", urlsBASIC);
		taboaAutorizacions.put("ADMIN", urlsADMIN);
		taboaAutorizacions.put("ANON", urlsANON);
	}
	private boolean isAuthorised(String _rol, String _url) {
		// recorro o Hashtable de roles e listas de urls autorizadas comprobando
		// se as urls autorizadas para o _rol do parámetro incluen a
		// url á que se desexa acceder, enviada como parámetro _url
		for (Map.Entry<String, String[]> entrada : taboaAutorizacions.entrySet()) {
			String rol = entrada.getKey();
			String[] urls = entrada.getValue();
			if (rol==_rol) {
				for (String url : urls) {
					//if (url.equals(_url)) {
					if(_url.startsWith(url)) {
                         // se coincide a url solicitada coa almacenada na taboa
                          // autorizo a petición.
						return true;
					}
				}
			}
		}
		if (_url.startsWith("/javax.faces.resource/")) { return true;}
		return false;
	}

}
