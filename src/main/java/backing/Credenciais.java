package backing;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ResourceBundle;
import java.util.UUID;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;

import javax.servlet.http.HttpServletRequest;

import modelo.ModeloDAO;
import modelo.Pedido;
import modelo.Usuario;

@Named
@SessionScoped
public class Credenciais implements Serializable {
	private static final ResourceBundle config= ResourceBundle.getBundle("config");
	// keys para config
	private static final String ROL_BASIC= "rol.BASIC";
	private static final String ROL_ANON= "rol.ANON";
	private static final String ROL_ADMIN= "rol.ADMIN";
	private static final String UPLOAD_DIR= "upload.dir";
	/////
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	private String confirmPassword;
	private String nome;
	private String rol;
	private boolean autenticado;
	private Usuario usuarioLogueado = null;
	boolean perfilValidado= false;
	private Part avatar;
	
	@Inject
	ModeloDAO modeloDAO;
	
	@Inject
	UserBacking userBacking;  // para cargar pedidos, entregas pendentes e o carro cando se loguea o usuario

	public Credenciais() {
		super();
		username = "";
		rol = config.getString(ROL_ANON);
		autenticado = false;
		usuarioLogueado = new Usuario();
		usuarioLogueado.setId(0);
		usuarioLogueado.setUsername("ANON");
		usuarioLogueado.setRol(config.getString(ROL_ANON));
		
	}

	public void rexistra() {
		FacesContext context = FacesContext.getCurrentInstance(); 
		// validar datos
		try {
			if (username == null || username.equals("") || modeloDAO.existeUsername(username)) {
				context.addMessage("rexistro:user", new FacesMessage("Username non válido")); 
			} else if (password==null || password.equals("")) {
				context.addMessage("rexistro:pass", new FacesMessage("Password non válido")); 
			} else if (!password.equals(confirmPassword)) {
				context.addMessage("rexistro:confirmpass", new FacesMessage("Passwords non coinciden "));
			}else if (nome==null || nome.equals("")) {
				context.addMessage("rexistro:nome", new FacesMessage("Nome non pode ser baleiro "));
			}else {
				context.addMessage("rexistro", new FacesMessage("Rexistro correcto"));
				Usuario usuario = new Usuario();
				usuario.setUsername(username);
				usuario.setPassword(password);
				usuario.setNome(nome);
				usuario.setRol(config.getString(ROL_BASIC));
				
				String fileName= avatar.getSubmittedFileName();
				if (fileName!=null && !fileName.equals("")) {
					String fileExtension = fileName.substring(fileName.indexOf('.'));
					fileName += "__"+UUID.randomUUID().toString()+fileExtension;
					try (InputStream input = avatar.getInputStream()) {
				        Files.copy(input, new File(config.getString(UPLOAD_DIR), fileName).toPath());
				    }catch (IOException e) {
				        // Show faces message?
				    }
					usuario.setAvatar(fileName);
				}else {
					usuario.setAvatar("default.png");
				}				
				modeloDAO.inserta(usuario);
			}
		} catch (Exception e) {

		}
	}

	public String autentica() {
		try {
			if (username != null && password != null && !username.equals("") && !password.equals("")) {
				if ((usuarioLogueado = modeloDAO.autentica(username, password)) != null) {
					FacesContext context = FacesContext.getCurrentInstance();
					((HttpServletRequest) context.getExternalContext().getRequest()).changeSessionId();
					rol = usuarioLogueado.getRol();
					nome = usuarioLogueado.getNome();
					autenticado = true;
					userBacking.cargaPedidos();  // cargo pedidos do usuario
					userBacking.cargaPedidosPendentesRecepcion();// carga entregas pendentes
					userBacking.cargaCarro(); // carga carro do usuario
					userBacking.cargaCatalogo();
					if (rol.equals(config.getString(ROL_ADMIN))) {
						return "/admin";
					}
				}
			}
//			System.out.println(this);
		} catch (Exception e) {

		}
		return "";
	}
	
 	public String logout() {
 		FacesContext.getCurrentInstance().getExternalContext().invalidateSession(); 
 		username = "";
		rol = config.getString(ROL_ANON);
		autenticado = false;
		usuarioLogueado = new Usuario();
		usuarioLogueado.setId(0);
		usuarioLogueado.setUsername("ANON");
		usuarioLogueado.setRol(config.getString(ROL_ANON));
		// borrar carro
		userBacking.carro = new Pedido();
		userBacking.carroBaleiro = true;
		// borrar pedidos pendentes de recepcion
		userBacking.pedidosPendentesRecepcion= null;
		System.out.println("Logout   =>  Rol:: ANON");
		return "index";
	}
 	
 	public void validateConfirmPassword(FacesContext context, UIComponent component,
 		        Object value) {
 		if (value==null) value="";
 		String confirmPassword =(String) value;
 		
 		// recuperamos o valor temporal do campo password.
 		UIInput passwordInput = (UIInput) component.findComponent("pass");
 	    String password = (String) passwordInput.getLocalValue();
 	    if (password==null)  password="";

 	    if (password == null || confirmPassword == null || !password.equals(confirmPassword)) {
 	        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "contrasinais non coinciden", "contrasinais non coinciden");
 	        throw new ValidatorException(msg);
 	    }
 	}
	
	public void actualiza() {
		FacesContext context = FacesContext.getCurrentInstance();
		if (password!= null && ! password.isEmpty()) {
			usuarioLogueado.setPassword(password);
		}
		if (avatar!=null) {
			String fileName= avatar.getSubmittedFileName();
			String fileExtension = fileName.substring(fileName.indexOf('.'));
			fileName += "__"+UUID.randomUUID().toString()+fileExtension;
			try (InputStream input = avatar.getInputStream()) {
		        Files.copy(input, new File(config.getString(UPLOAD_DIR), fileName).toPath());
		    }catch (IOException e) {
		        // Show faces message?
		    }
			// borrar anterior avatar
			new File(config.getString(UPLOAD_DIR)+usuarioLogueado.getAvatar()).delete();
			usuarioLogueado.setAvatar(fileName);
		}
		System.out.println("credenciais:actualiza..usuarioLogueado: "+usuarioLogueado);
		try {
				modeloDAO.actualiza(usuarioLogueado);
				context.addMessage("perfil",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Usuario actualizado con éxito"));
		} catch (Exception e) {
				e.printStackTrace();
		}
	}
	//////////////////////////////////////////7
	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Usuario getUsuarioLogueado() {
		return usuarioLogueado;
	}

	public void setUsuarioLogueado(Usuario usuarioLogueado) {
		this.usuarioLogueado = usuarioLogueado;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public boolean isAutenticado() {
		return autenticado;
	}

	public void setAutenticado(boolean autenticado) {
		this.autenticado = autenticado;
	}

	public boolean isPerfilValidado() {
		return perfilValidado;
	}

	public void setPerfilValidado(boolean perfilValidado) {
		this.perfilValidado = perfilValidado;
	}

	public Part getAvatar() {
		return avatar;
	}
	
	public void setAvatar(Part avatar) {
		this.avatar = avatar;
	}
	@Override
	public String toString() {
		return "Credenciais [username=" + username + ", password=" + password + ", rol=" + rol + ", autenticado="
				+ autenticado + "]";
	}
}
