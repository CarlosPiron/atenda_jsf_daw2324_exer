package modelo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

//import com.fasterxml.jackson.annotation.JsonFormat;
//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
//import com.fasterxml.jackson.databind.annotation.JsonSerialize;
//import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
//import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

public class Pedido {
	private int id;
	private int idPedidoOrix;
	private Usuario cliente;
	
//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss") 
//	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
//	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime data;
	private ArrayList<LineaPedido> lineasPedido = new ArrayList<LineaPedido>();
	private boolean pechado;
	private boolean recibido;
	public Pedido() {
		super();
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdPedidoOrix() {
		return idPedidoOrix;
	}

	public void setIdPedidoOrix(int idPedidoOrix) {
		this.idPedidoOrix = idPedidoOrix;
	}

	public Usuario getCliente() {
		return cliente;
	}

	public void setCliente(Usuario cliente) {
		this.cliente = cliente;
	}

	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}
	public ArrayList<LineaPedido> getLineasPedido() {
		return lineasPedido;
	}

	public void setLineasPedido(ArrayList<LineaPedido> lineasPedido) {
		this.lineasPedido = lineasPedido;
	}
	public void addLineaPedido (LineaPedido linea) {
		lineasPedido.add(linea);
	}
	public void removeLineaPedido (LineaPedido linea) {
		// non identifico lineapedido con id_lineaPedido; porque igual non o ten a�nda asignado
		// identifico lineapedido polo producto. Asumo que todas as li�as pedido te�en
		// produtos distintos
		for (int i =0; i<lineasPedido.size(); i++) {
			if (lineasPedido.get(i).getProduto().equals(linea.getProduto())) {
				lineasPedido.remove(i);
			}
		}	
	}
	
	//@JsonIgnore()   // non se serializa
	public double getImporteConIva () {
		double importeConIva=0;
		for (LineaPedido linea: lineasPedido) {
			importeConIva += linea.getProduto().getPrezo() *(1+(linea.getProduto().getIva())/100.0) *linea.getUnidades();
		}
		return (Math.round(importeConIva * 100) / 100.0);
	}
	public boolean isPechado() {
		return pechado;
	}
	public void setPechado(boolean pechado) {
		this.pechado = pechado;
	}
	
	public boolean isRecibido() {
		return recibido;
	}
	public void setRecibido(boolean recibido) {
		this.recibido = recibido;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		String resultado = "Pedido [id=" + id + ", idPedidoOrix=" + idPedidoOrix + ", cliente=" + cliente.getId() + ", data=" + data+", pechado= "+pechado+", recibido= "+recibido+System.lineSeparator();
		for (LineaPedido linea: lineasPedido) {
			resultado += linea.toString()+System.lineSeparator();
		}
		return resultado;
	}
}
