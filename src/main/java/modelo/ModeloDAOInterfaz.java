package modelo;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public interface ModeloDAOInterfaz {
	 public static final String DEFAULT_FOTO="default.PNG";
	 public static final String UPLOAD_DIR ="c:/ficheros/images/";
//	 public static final String UPLOAD_DIR ="/atenda/uploads";
	 public static final LocalDate INICIO_REXISTRO_PEDIDOS = LocalDate.now().minusYears(5); // data de inicio dos pedidos
	 
	 // UsuarioDAO
	 public Usuario autentica(String username, String password ) throws Exception;// devolve o usuario se autenticaci�n correcta ou null en caso contrario
	 public ArrayList<Usuario> getAllUsers() throws Exception;// devolve a lista de usuarios 
	 public void actualiza(Usuario usuario) throws Exception; // actualiza usuario
	 public int inserta(Usuario usuario) throws Exception; // inserta usuario e devolve id
	 public void borra (Usuario usuario) throws Exception; // borra usuario
	 public boolean existe (Usuario usuario) throws Exception; // mira se usuario existe
	 public boolean existeUsername (String username) throws Exception; // mira se existe username
	 public Usuario getUsuarioPorId (int idUsuario) throws Exception;// obten usuario polo id
	 // ProdutoDAO
	 public ArrayList<Produto> getAll() throws Exception; // devolve a lista de produtos
	 public void actualiza(Produto produto) throws Exception; // actualiza produto
	 public int inserta(Produto produto) throws Exception;//inserta produto
	 public Produto insertaVacio() throws Exception;//inserta produto vacio
	 public void borra (Produto produto) throws Exception; // borra produto
	 public Produto getProdutoPorId(int idProduto) throws Exception; // obten produto por id
	 // XML
	 public void exportaProdutos(ArrayList<Produto> produtos, File exportedFile); // exporta a lista de productos a o ficheiro en formato XML
	 public void importaProdutos (File xmlFileProdutos); //importa os productos do ficheiro na lista de produtos
	 // PedidoDAO
	 public Pedido getPedidoPorId(int id) throws Exception; // devolve pedido de campo id= id
	// devolve os pedidos abertos ou pechados entre as d�as datas con ou sin devoluci�ns e recibidos ou non
	 public ArrayList<Pedido> getPedidosPeriodo(LocalDate dende, LocalDate ata, boolean conDevolucions, boolean pechado, boolean recibido) throws Exception;
	// devolve os pedidos abertos ou pechados entre as d�as datas dun usuario e con ou sin devoluci�ns e recibidos ou non
	 public ArrayList<Pedido> getPedidosPeriodoDe(LocalDate dendeLocalDate, LocalDate ataLocalDate, boolean eDevolucions,
				Usuario usuario, boolean pechado, boolean recibido) throws Exception; 
	 public ArrayList<Pedido> getDevolucionsDe (Pedido pedido)throws Exception; // devolve a lista de pedidos devoluci�ns do pedido parametro
	 public int inserta(Pedido pedido)throws Exception; // // inserta pedido como aberto, insertando tam�n lineas pedido que cont�n
	 public void actualiza(Pedido pedido )throws Exception; // actualiza todo menos lineas pedido
	 // LineaPedidoDAO
	 public int inserta (LineaPedido lineaPedido, Pedido pedido) throws Exception; // inserta LineaPedido e devolve id
	 public void actualiza (LineaPedido lineaPedido) throws Exception; // actualiza LineaPedido (unidades só)
	 public int getUnidadesDevoltasDe (LineaPedido lineaPedido) throws Exception;   // devolve as devoluci�ns xa feitas dunha li�a pedido
	 // OpinionDAO
	 public int getValoracionMedia(Produto produto) throws Exception; // obten a valoración media enteira dun produto 
	 public ArrayList<Opinion>  getOpinions(Produto produto) throws Exception ; // devolve as opinions sobre un produto
	 public LinkedHashMap<Integer, Integer> getValoracions (Produto produto) throws Exception; // devolve as valoracións sobre un produto
	  																							// como pares valoracion-numero de valoracions
	 public int inserta(Opinion comentario) throws Exception; // insertar unha opinión na base de datos
	 // BI
	 public Informe getInformePedidos (ArrayList<Pedido> pedidos ) throws Exception; // devolve o infome da lista de pedidos par�metro
	
}
